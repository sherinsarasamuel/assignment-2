from random import randint
            
theBoard = {'1': ' ' , '2': ' ' , '3': ' ' ,
            '4': ' ' , '5': ' ' , '6': ' ' ,
            '7': ' ' , '8': ' ' , '9': ' ' }

board_keys = []

for key in theBoard:
    board_keys.append(key)

''' We will have to print the updated board after every move in the game and 
    thus we will make a function in which we'll define the printBoard function
    so that we can easily print the board everytime by calling this function. '''

def printBoard(board):
    print(board['1'] + '|' + board['2'] + '|' + board['3'])
    print('-+-+-')
    print(board['4'] + '|' + board['5'] + '|' + board['6'])
    print('-+-+-')
    print(board['7'] + '|' + board['8'] + '|' + board['9'])

# Now we'll write the main function which has all the gameplay functionality.
def game():
    
    history={}
    result='tie'

    for j in range (1,10):
        turn = 'X'
        count = 0
        while count<=9:
            if(turn=='X'):
                printBoard(theBoard)
                print("It's your turn," + turn + ".Move to which place?")
        
                move = input()
                print(count)
            else:
                move=str(randint(1,9))
    
            if theBoard[move] == ' ':
                theBoard[move] = turn
                count += 1
            else:
                print("That place is already filled.\nMove to which place?")
                continue
    
            # Now we will check if player X or O has won,for every move after 5 moves. 
            if count >= 5:
                if theBoard['7'] == theBoard['8'] == theBoard['9'] != ' ' or theBoard['4'] == theBoard['5'] == theBoard['6'] != ' ' or theBoard['1'] == theBoard['2'] == theBoard['3'] != ' ' or theBoard['1'] == theBoard['4'] == theBoard['7'] != ' ' or theBoard['2'] == theBoard['5'] == theBoard['8'] != ' ' or theBoard['3'] == theBoard['6'] == theBoard['9'] != ' ' or theBoard['7'] == theBoard['5'] == theBoard['3'] != ' ' or theBoard['1'] == theBoard['5'] == theBoard['9'] != ' ': 
                    printBoard(theBoard)
                    print("\nGame Over.\n")                
                    print(" **** " +turn + " won. ****") 
                    result=turn
                    break
    
            # If neither X nor O wins and the board is full, we'll declare the result as 'tie'.
            if count == 9:
                print("\nGame Over.\n")                
                print("It's a Tie!!")
                result='Tie'
    
            # Now we have to change the player after every move.
            if turn =='X':
                turn = 'O'
            else:
                turn = 'X'        
        
        # Now we will ask if player wants to restart the game or not.
        if(j!=10):
            restart = input("Do want to play Again?(y/n)")
            history[j]=[theBoard,result]
            if restart == "y" or restart == "Y":
                for key in board_keys:
                    theBoard[key] = " "
        
                continue
            else:
                print('History: ', history)
                break
                
if __name__ == "__main__":
    
    game()